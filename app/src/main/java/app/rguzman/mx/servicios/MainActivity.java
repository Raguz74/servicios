package app.rguzman.mx.servicios;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    public final static String EXTRA_ID_SERVICIOS = "SERVICIOS.ID_SERVICIOS";
    private SimpleCursorAdapter adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listaDeServicios = (ListView) findViewById(R.id.activity_main);
        listaDeServicios.setOnItemClickListener(this);

        adaptador = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                null,
                new String[]{ServiciosDatabase.COL_CLIENTE},
                new int[]{android.R.id.text1},
                0);

        listaDeServicios.setAdapter(adaptador);

        getSupportLoaderManager().initLoader(0,null,this);

        findViewById(R.id.boton_nuevo_servicio).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intento = new Intent(MainActivity.this, AgregarServicios.class);
                startActivity(intento);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intencion = new Intent(this,DetalleServicios.class) ;
        intencion.putExtra(EXTRA_ID_SERVICIOS,adaptador.getItemId(position));
        startActivity(intencion);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ServiciosLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adaptador.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adaptador.swapCursor(null);
    }
}