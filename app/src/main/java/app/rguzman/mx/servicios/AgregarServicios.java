package app.rguzman.mx.servicios;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import java.lang.ref.WeakReference;

public class AgregarServicios extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private long serviciosId;
    private EditText clienteEditText;
    private EditText direccionEditText;
    private EditText telefonoEditText;
    private EditText incidenteEditText;
    private EditText tecnicoEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_servicios);

        clienteEditText = (EditText) findViewById(R.id.cliente_edit_text);
        direccionEditText = (EditText) findViewById(R.id.direccion_edit_text);
        telefonoEditText = (EditText) findViewById(R.id.telefono_edit_text);
        incidenteEditText = (EditText) findViewById(R.id.incidente_edit_text);
        tecnicoEditText = (EditText) findViewById(R.id.tecnico_edit_text);

        serviciosId = getIntent().getLongExtra(DetalleServicios.EXTRA_SERVICIOS_ID, -1L);
        if (serviciosId != -1L) {
            getSupportLoaderManager().initLoader(0, null, this);
        }

        findViewById(R.id.boton_agregar).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String clienteServicios = clienteEditText.getText().toString();
        String direccionServicios = direccionEditText.getText().toString();
        String telefonoServicios = telefonoEditText.getText().toString();
        String incidenteServicios = incidenteEditText.getText().toString();
        String tecnicoServicios = tecnicoEditText.getText().toString();

        if (clienteServicios.isEmpty() || direccionServicios.isEmpty() || incidenteServicios.isEmpty())
        {Toast.makeText(this, "El nombre del cliente, la dirección y el reporte son obligatorios", Toast.LENGTH_SHORT).show();}
        else {

        new CreateServiciosTask(this,clienteServicios,direccionServicios,telefonoServicios,incidenteServicios,tecnicoServicios,serviciosId).execute();}
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ServicioLoader(this,serviciosId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data!=null && data.moveToFirst()){
            int clienteIndex=data.getColumnIndexOrThrow(ServiciosDatabase.COL_CLIENTE);
            String clienteServicios=data.getString(clienteIndex);

            int direccionIndex=data.getColumnIndexOrThrow(ServiciosDatabase.COL_DIRECCION);
            String direccionServicios=data.getString(direccionIndex);

            int telefonoIndex=data.getColumnIndexOrThrow(ServiciosDatabase.COL_TELEFONO);
            String telefonoServicios=data.getString(telefonoIndex);

            int incidenteIndex=data.getColumnIndexOrThrow(ServiciosDatabase.COL_INCIDENTE);
            String incidenteServicios=data.getString(incidenteIndex);

            int tecnicoIndex=data.getColumnIndexOrThrow(ServiciosDatabase.COL_TECNICO);
            String tecnicoServicios=data.getString(tecnicoIndex);

            clienteEditText.setText(clienteServicios);
            direccionEditText.setText(direccionServicios);
            telefonoEditText.setText(telefonoServicios);
            incidenteEditText.setText(incidenteServicios);
            tecnicoEditText.setText(tecnicoServicios);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
    public static class CreateServiciosTask extends AsyncTask<Void, Void, Boolean> {
        private long serviciosId;
        private WeakReference<Activity> weakActivity;
        private String serviciosCliente;
        private String serviciosDireccion;
        private String serviciosTelefono;
        private String serviciosIncidente;
        private String serviciosTecnico;

        public CreateServiciosTask(Activity activity, String cliente, String direccion, String telefono, String incidente, String tecnico,  long serviciosId){
            weakActivity = new WeakReference<Activity>(activity);
            serviciosCliente = cliente;
            serviciosDireccion = direccion;
            serviciosTelefono= telefono;
            serviciosIncidente = incidente;
            serviciosTecnico= tecnico;
            this.serviciosId = serviciosId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();
            boolean succes = false;

            if (serviciosId!=-1L){
                int filasAfectadas=ServiciosDatabase.actualizaServicios(appContext, serviciosCliente, serviciosDireccion, serviciosTelefono, serviciosIncidente, serviciosTecnico,serviciosId);
                succes=(filasAfectadas != 0);
            } else {
                long id=ServiciosDatabase.insertServicios(appContext, serviciosCliente, serviciosDireccion, serviciosTelefono, serviciosIncidente, serviciosTecnico);
                succes=(id != -1L);
            }

            return  succes;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }
    }
}