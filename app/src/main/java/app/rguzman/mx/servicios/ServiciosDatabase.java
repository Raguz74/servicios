package app.rguzman.mx.servicios;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.v4.content.LocalBroadcastManager;


public class ServiciosDatabase extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="servicios.db";
    private static final String TABLE_NAME="servicios";
    public  static final String COL_CLIENTE="cliente";
    public  static final String COL_DIRECCION="direccion";
    public  static final String COL_TELEFONO="telefono";
    public  static final String COL_INCIDENTE="incidente";
    public  static final String COL_TECNICO="tecnico";

    public ServiciosDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createQuery = "CREATE TABLE " + TABLE_NAME +
                " (_id INTEGER PRIMARY KEY, "
                + COL_CLIENTE+" TEXT NOT NULL COLLATE UNICODE, "
                + COL_DIRECCION+" TEXT NOT NULL, "
                + COL_TELEFONO+" TEXT NOT NULL, "
                + COL_INCIDENTE+" TEXT NOT NULL, "
                + COL_TECNICO+" TEXT NOT NULL)";
        db.execSQL(createQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String upgradeQuery = "DROP TABLE IF EXISTS "+TABLE_NAME;
        db.execSQL(upgradeQuery);
    }

    public static long insertServicios(Context context, String cliente, String direccion, String telefono, String incidente, String tecnico){
        SQLiteOpenHelper dbOpenHelper = new ServiciosDatabase(context);
        SQLiteDatabase database= dbOpenHelper.getWritableDatabase();
        ContentValues valorServicios=new ContentValues();
        valorServicios.put(COL_CLIENTE,cliente);
        valorServicios.put(COL_DIRECCION,direccion);
        valorServicios.put(COL_TELEFONO,telefono);
        valorServicios.put(COL_INCIDENTE,incidente);
        valorServicios.put(COL_TECNICO,tecnico);

        long result=-1L;

        try {
            result= database.insert(TABLE_NAME,null,valorServicios);
            if (result != -1L){
                LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                Intent intentFilter = new Intent(ServiciosLoader.ACTION_RELOAD_TABLE);
                broadcastManager.sendBroadcast(intentFilter);
            }

        } finally {
            dbOpenHelper.close();
        }
//        long result = database.insert(TABLE_NAME,null,valorAnimal);
//        dbOpenHelper.close();
        return result;
    }

    public static Cursor devuelveTodos (Context context){
        SQLiteOpenHelper dbOpenHelper = new ServiciosDatabase(context);
        SQLiteDatabase database= dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME, new String[]{COL_CLIENTE,COL_DIRECCION, COL_TELEFONO, COL_INCIDENTE, COL_TECNICO, BaseColumns._ID}
                ,null,null,null,null,
                COL_CLIENTE+" ASC");
    }

    public static Cursor devuelveConId(Context context, long identificador){
        SQLiteOpenHelper dbOpenHelper = new ServiciosDatabase(context);
        SQLiteDatabase database= dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME, new String[]{COL_CLIENTE,COL_DIRECCION,COL_TELEFONO,COL_INCIDENTE,COL_TECNICO,BaseColumns._ID},
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(identificador)},
                null,
                null,
                COL_CLIENTE +" ASC");
    }

    public static int eliminaConId(Context context, long serviciosId){
        SQLiteOpenHelper dbOpenHelper = new ServiciosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        int resultado = database.delete(
                TABLE_NAME,BaseColumns._ID + " =?",
                new String[]{String.valueOf(serviciosId)});

        if (resultado != 0){
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(ServiciosLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }
        dbOpenHelper.close();
        return resultado;
    }

    public static int actualizaServicios(Context context, String cliente,
                                        String direccion, String telefono, String incidente, String tecnico, long serviciosId){
        SQLiteOpenHelper dbOpenHelper = new ServiciosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valorServicios = new ContentValues();
        valorServicios.put(COL_CLIENTE, cliente);
        valorServicios.put(COL_DIRECCION, direccion);
        valorServicios.put(COL_TELEFONO, telefono);
        valorServicios.put(COL_INCIDENTE, incidente);
        valorServicios.put(COL_TECNICO, tecnico);

        int result = database.update(
                TABLE_NAME,
                valorServicios, BaseColumns._ID + " =?",
                new String[]{String.valueOf(serviciosId)});

        if (result != 0){
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(ServiciosLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }
        dbOpenHelper.close();
        return result;
    }
}
