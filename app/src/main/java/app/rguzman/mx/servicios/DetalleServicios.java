package app.rguzman.mx.servicios;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.ref.WeakReference;

public class DetalleServicios extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private long serviciosId;
    private TextView clienteTextView;
    private TextView direccionTextView;
    private TextView telefonoTextView;
    private TextView incidenteTextView;
    private TextView tecnicoTextView;
    public static final String EXTRA_SERVICIOS_ID="servicios.id.extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_servicios);

        clienteTextView = (TextView) findViewById(R.id.cliente_text_view);
        direccionTextView=(TextView) findViewById(R.id.direccion_text_view);
        telefonoTextView=(TextView) findViewById(R.id.telefono_text_view);
        incidenteTextView=(TextView) findViewById(R.id.incidente_text_view);
        tecnicoTextView=(TextView) findViewById(R.id.tecnico_text_view);

        Intent intencion = getIntent();

        serviciosId=intencion.getLongExtra(MainActivity.EXTRA_ID_SERVICIOS,-1L);

        getSupportLoaderManager().initLoader(0,null,this);

        findViewById(R.id.boton_eliminar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DeleteServiciosTask(DetalleServicios.this,serviciosId).execute();
                    }
                }
        );

        findViewById(R.id.boton_actualizar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(DetalleServicios.this,AgregarServicios.class);
                        intent.putExtra(EXTRA_SERVICIOS_ID,serviciosId);
                        startActivity(intent);
                    }
                }
        );
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ServicioLoader(this,serviciosId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data!=null && data.moveToFirst()){
            int clienteIndex=data.getColumnIndexOrThrow(ServiciosDatabase.COL_CLIENTE);
            String clienteServicios=data.getString(clienteIndex);

            int direccionIndex=data.getColumnIndexOrThrow(ServiciosDatabase.COL_DIRECCION);
            String direccionServicios=data.getString(direccionIndex);

            int telefonoIndex=data.getColumnIndexOrThrow(ServiciosDatabase.COL_TELEFONO);
            String telefonoServicios=data.getString(telefonoIndex);

            int incidenteIndex=data.getColumnIndexOrThrow(ServiciosDatabase.COL_INCIDENTE);
            String incidenteServicios=data.getString(incidenteIndex);

            int tecnicoIndex=data.getColumnIndexOrThrow(ServiciosDatabase.COL_TECNICO);
            String tecnicoServicios=data.getString(tecnicoIndex);

            clienteTextView.setText(clienteServicios);
            direccionTextView.setText(direccionServicios);
            telefonoTextView.setText(telefonoServicios);
            incidenteTextView.setText(incidenteServicios);
            tecnicoTextView.setText(tecnicoServicios);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    public static class DeleteServiciosTask extends AsyncTask<Void, Void, Boolean> {
        private WeakReference<Activity> weakActivity;
        private Long serviciosId;

        public DeleteServiciosTask(Activity activity, long id){
            weakActivity = new WeakReference<Activity>(activity);
            serviciosId=id;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }
            Context appContext = context.getApplicationContext();
            int filasAfectadas = ServiciosDatabase.eliminaConId(appContext, serviciosId);

            return  (filasAfectadas != 0);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }
    }
}